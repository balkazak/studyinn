$( document ).ready(function() {
  $('.license-list').slick({
      centerMode: true,
      slidesToShow: 3,
      prevArrow: '<img src="Images/slick-left-arrow.png" class="slick-arrows slick-arrow-left"/>',
      nextArrow: '<img src="Images/slick-left-arrow.png" class="slick-arrows slick-arrow-right"/>',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 575,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
  });
  $('.partner-logos').slick({
      infinite: true,
      slidesToShow: 4,
      prevArrow: '<img src="Images/icon-blue/slick-left-arrow-blue.png" class="slick-arrows slick-arrow-left"/>',
      nextArrow: '<img src="Images/icon-blue/slick-left-arrow-blue.png" class="slick-arrows slick-arrow-right"/>',
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 2
          }
        }
      ]
  });
  $('.news-row').slick({
    infinite: true,
    slidesToShow: 3,
    prevArrow: '<img src="Images/slick-left-arrow-white.png" class="slick-arrows slick-arrow-left"/>',
    nextArrow: '<img src="Images/slick-left-arrow-white.png" class="slick-arrows slick-arrow-right"/>',
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1
        }
      }
    ]
});

$('#section-geo .city').click(function() {
  var city = $(this).data('city');
  $('.city-wrapper').removeClass('active')
  $('#' + city).addClass('active')
});
$('.questions-types .questions-type').click(function() {
  var type = $(this).data('type');
  $('.questions-types .questions-type').removeClass('active')
  $(this).addClass('active')
  $('.question-type-form').removeClass('active')
  $('#form-' + type).addClass('active')
});

$('.navigation-mobile .menu').click(function() {
  $('.mobile-sidebar').addClass('active');
  $('.sidebar-content').addClass('active');
  $('.sidebar-closer').addClass('active');
})
$('.sidebar-closer, .sidebar-content a').click(function() {
  $('.mobile-sidebar').removeClass('active');
  $('.sidebar-content').removeClass('active');
  $('.sidebar-closer').removeClass('active');
})

$('.city-select').change(function() {
  var selectedCity = $('.city-select').val();
  $('.section-contacts .address-list, .section-contacts .map').removeClass('active');
  $('.city-map-' + selectedCity).addClass('active');
  $('.city-address-' + selectedCity).addClass('active');
});

$('.image-popup-vertical-fit').magnificPopup({
  type: 'image',
  closeOnContentClick: true,
  mainClass: 'mfp-img-mobile',
  image: {
    verticalFit: true
  }

});

$('.image-popup-fit-width').magnificPopup({
  type: 'image',
  closeOnContentClick: true,
  image: {
    verticalFit: false
  }
});

$('.image-popup-no-margins').magnificPopup({
  type: 'image',
  closeOnContentClick: true,
  closeBtnInside: false,
  fixedContentPos: true,
  mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
  image: {
    verticalFit: true
  },
  zoom: {
    enabled: true,
    duration: 300 // don't foget to change the duration also in CSS
  }
});

$('.popup-with-form').magnificPopup({
  type: 'inline',
  preloader: false,
  focus: '#name',

  callbacks: {
    beforeOpen: function() {
      if($(window).width() < 700) {
        this.st.focus = false;
      } else {
        this.st.focus = '#name';
      }
    }
  }
});

var a = 0;
$(window).scroll(function() {

  var oTop = $('#section-statistics .statistics-list').offset().top - window.innerHeight;
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.counter-value').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },

        {

          duration: 4000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});
});

$(document).ready(function(){
  $('#section-main .sections-list .sections-item').click(function() {
      $('#section-main .sections-list .sections-item').removeClass('active');
      $(this).addClass('active');
  })

  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
      // On-page links
      if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
      ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body').animate({
          scrollTop: target.offset().top
          }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
              return false;
          } else {
              $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
          };
          });
      }
      }
  });
});

$(document).ready(function () {

$('.callbackForm').submit(function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
    var form = this;

    $.ajax({
        type: 'POST',
        url: 'mail.php',
        data: formData,
        async: true,
        beforeSend: function() {
            $(form).find(' input[type=submit]').html('Sending message');

            $(form).attr('disabled', true);
            $(form.elements).attr('disabled', true);
        },
        success: function(data) {
            $(form).attr('disabled', false);
            $(form.elements).attr('disabled', false);
            $(form).find(' input[type=submit]').html('Message sended');
            $(form).find("input[type=text], textarea").val("");
            $(form).css('display', 'none');
            $(form).parent().find('.callbackText').css('display', 'block');
        },
        error: function(xhr, status, error){
            console.log(xhr);
        }
    });
});

});